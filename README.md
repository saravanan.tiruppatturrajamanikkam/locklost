locklost: aLIGO IFO lock loss tracking and analysis tool
========================================================

This package provides a set of tools for analyzing LIGO detector "lock
losses".  It consists of four main components:

* `search` for detector lock losses based on guardian state
  transitions.
* `analyze` lock losses to generate plots and look for identifying
  features.
* `online` analysis to monitor for lock losses and automatically run
  follow-up analyses.
* `web` interface to view lock loses event pages.

A command line interface is provided to launch the `online` analysis
and condor jobs to `search` for and `analyze` events.


# Deployments

Each site (LHO and LLO) has a dedicated "lockloss" account on the site
LDAS cluster where the primary lock loss tracking via `locklost` takes
place:

* https://ldas-jobs.ligo-wa.caltech.edu/~lockloss/
* https://ldas-jobs.ligo-la.caltech.edu/~lockloss/

These accounts have deployments of the `locklost` package and command
line interface, run the online and followup condor jobs, and host the
web pages.

## deploying new versions

When a new tag has been applied to the code, the following can be run
to deploy the new code at the site:
```shell
$ ssh lockloss@detchar.ligo-la.caltech.edu
$ cd src/locklost
$ git pull
$ locklost-deploy
```
If there are changes to the online search, restart the online condor
process:
```shell
$ locklost online restart
```


# Usage

To start/stop/restart the online analysis use the `online` command:
```shell
$ locklost online start
```
This launches a condor job that runs the online analysis.

To launch a condor job to search for lock losses within some time
window:
```shell
$ locklost search GPS_START GPS_END
```
This will find lock losses with the specified time range, but will not
run the follow-up analyses.  This is only needed to cover times when
the online analysis was not running.

To run an full analysis on a specific lock loss time found from the
search above:
```shell
$ locklost analyze GPS_TIME
```
To launch a condor job to analyze all events within a time range:
```shell
$ locklost analyze --condor GPS_START GPS_END
```
To re-analyze events add the `--rerun` flag:
```shell
$ locklost analyze --condor GPS_START GPS_END --rerun
```


# Analysis plugins

Lock loss event analyses are handled by a set of "plugins" located in
the /locklost/plugins/ package directory:

* [`locklost/plugins/`](/locklost/plugins/)

Each module is registered in [`locklost/plugins/__init__.py`](/locklost/plugins/__init__.py).  The
currently enabled follow-up plugins are:

* `discover.discover_data` wait for data to be available
* `refine.refine_event` refine event time
* `saturations.find_saturations` find saturating channels before event
* `lpy.find_lpy` find length/pitch/yaw oscillations in the suspensions
* `glitch.analyze_glitches` look for glitches around event
* `overflows.find_overflows` look for ADC overflows
* `state_start.find_lock_start` find the start of the lock

Each plugin does it's own analysis, although some depend on the output
of other analyses.  The output from any analysis (e.g. plots or data)
should be written into the event directory.


# Developing and contributing

See the [CONTRIBUTING](CONTRIBUTING.md) for instructions on how to
contribute to `locklost`.
