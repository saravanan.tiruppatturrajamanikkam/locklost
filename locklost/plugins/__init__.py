import collections
from gwpy.segments import Segment
import matplotlib.pyplot as plt
from matplotlib import rcParamsDefault
plt.rcParams.update(rcParamsDefault)

followups = collections.OrderedDict()
def add_follow(mod):
    followups.update([(mod.__name__, mod)])

from .discover import discover_data
add_follow(discover_data)

from .refine import refine_event
add_follow(refine_event)

from .saturations import find_saturations
add_follow(find_saturations)

from .lpy import find_lpy
add_follow(find_lpy)

from .lsc_asc import plot_lsc_asc
add_follow(plot_lsc_asc)

from .glitch import analyze_glitches
add_follow(analyze_glitches)

from .overflows import find_overflows
add_follow(find_overflows)

from .previous_state import find_previous_state
add_follow(find_previous_state)

from .observe import check_observe
add_follow(check_observe)
