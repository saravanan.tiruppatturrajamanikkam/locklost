import os
import logging
import argparse
import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import config
from ..event import LocklossEvent
from .. import data

##################################################

def plot_indicators(event, bufs, params, refined_gps=None, threshold=None):
    gps = int(event.transition_gps)
    trans = event.transition_index

    y_grd, t_grd = bufs[0].yt()
    y_ind, t_ind = bufs[1].yt()

    plt.rcParams['text.usetex'] = False

    isc_loss_time = event.transition_gps - gps

    for window_type, window in config.REFINE_PLOT_WINDOWS.items():
        logging.info('Making {} indicator plot'.format(window_type))

        # filter data based on window
        condition = np.logical_and(window[0] <= t_ind-gps, t_ind-gps <= window[1])
        idx = np.argwhere(condition)
        y = y_ind[idx]
        t = t_ind[idx]

        fig, ax1 = plt.subplots()
        ax2 = ax1.twinx()
        # this puts ax1 on top
        ax1.set_zorder(10)
        # sets ax1 background invisible so the lower axis shows through
        ax1.patch.set_visible(False)

        color = 'blue'
        label = params['CHANNEL']
        ax1.plot(t-gps, y, label=label,
                 color=color, linewidth=2)
        ax1.grid(True)
        ax1.set_ylabel(params['CHANNEL'])
        ax1.yaxis.label.set_color(color)
        ax1.set_xlabel('Time [s] since lock loss at {}'.format(gps))

        # filter data based on window
        condition = np.logical_and(window[0] <= t_grd-gps, t_grd-gps <= window[1])
        idx = np.argwhere(condition)
        y = y_grd[idx]
        t = t_grd[idx]

        color = 'orange'
        label = config.GRD_STATE_N_CHANNEL
        ax2.plot(t-gps, y, label=label,
                 color=color, linewidth=2)
        ax2.set_yticks(trans)
        ylim = ax2.get_ylim()
        ax2.set_ylim([ylim[0]-10, ylim[1]+10])
        ax2.set_ylabel(config.GRD_STATE_N_CHANNEL)
        ax2.yaxis.label.set_color(color)

        # add annotation/line for threshold
        if threshold:
            ax1.axhline(
                threshold,
                label='threshold',
                color='green',
                linestyle='--',
                linewidth=2,
            )
            if params['THRESHOLD'] > 0:
                xyoffset = (8, 10)
                valign = 'bottom'
            else:
                xyoffset = (8, -10)
                valign = 'top'
            ax1.annotate(
                "threshold",
                xy=(ax1.get_xlim()[0], threshold),
                xycoords='data',
                xytext=xyoffset,
                textcoords='offset points',
                horizontalalignment='left',
                verticalalignment=valign,
                bbox=dict(
                    boxstyle="round", fc="w", ec="green", alpha=0.95),
                )

        if refined_gps:
            atx = refined_gps-gps
            annotation = "refined time: {}".format(refined_gps)
        else:
            atx = 0
            annotation = "unable to refine time"

        # add annotation/line for lock loss time
        ax1.axvline(
            atx,
            label='refined time',
            color='red',
            linestyle='--',
            linewidth=2,
        )
        ax1.annotate(
            annotation,
            xy=(atx, ax1.get_ylim()[1]),
            xycoords='data',
            xytext=(0, 2),
            textcoords='offset points',
            horizontalalignment='center',
            verticalalignment='bottom',
            bbox=dict(boxstyle="round", fc="w", ec="red", alpha=0.95),
            )
        ax2.axhline(
            trans[0],
            label = 'state before lockloss',
            color = 'orange',
            linestyle = '--',
            linewidth = 2
        )
        ax2.axvline(
            isc_loss_time,
            label = 'lockloss time',
            color = 'orange',
            linestyle = '--',
            linewidth = 2
        )

        fig.suptitle("Lock loss refinement at {}".format(str(event)))

        outfile = 'indicators_{}.png'.format(window_type)
        outpath = os.path.join(event.path, outfile)
        logging.debug(outpath)
        fig.savefig(outpath)


def refine_event(event):
    """Refine lock loss event time

    """
    # clean up from any previous runs
    try:
        os.remove(event.gen_path('refined_gps'))
    except OSError:
        pass
    event.rm_tag('REFINED')

    ifo = config.IFO
    gps = int(event.gps)
    refined_gps = None

    # find indicator channel to use based on guardian state
    for index, params in sorted(config.INDICATORS[config.IFO].items(), reverse=True):
        if event.transition_index[0] >= index:
            break

    channels = [
        config.GRD_STATE_N_CHANNEL,
        params['CHANNEL'],
    ]

    window = config.REFINE_PLOT_WINDOWS['WIDE']
    segment = Segment(*window).shift(gps)

    bufs = data.fetch(channels, segment)

    ibuf = bufs[1]

    # assume the IFO is locked up to 5 seconds of segment and
    # find mean and stddev

    delta_t = np.abs(window[0]) - 5
    samples_before = int(ibuf.sample_rate * delta_t)

    # calculate mean and std
    data_before_lock = ibuf.data[:samples_before]
    lock_mean = np.mean(ibuf.data[:samples_before])
    lock_stdd = np.std(ibuf.data[:samples_before] - lock_mean, ddof=1)
    logging.info("locked mean/stddev: {}/{}".format(lock_mean, lock_stdd))

    # lock loss threshold is when moves past % threshold of std
    threshold = lock_stdd * params['THRESHOLD'] + lock_mean
    logging.info("threshold: {}".format(threshold))

    # if the mean is less than the nominal full lock value then abort,
    # as this isn't a clean lock
    if lock_mean < params['MINIMUM']:
        logging.info("unable to resolve lockloss time")

    else:
        if params['THRESHOLD'] > 0:
            inds = np.where(ibuf.data > threshold)[0]
        else:
            inds = np.where(ibuf.data < threshold)[0]
        if inds.any():
            ind = np.min(inds)
            refined_gps = ibuf.timeseries[ind]
            logging.info("refined time: {}".format(refined_gps))

            if refined_gps > event.transition_gps:

                logging.info(
                    "refined gps past lock loss time, unable to resolve"
                )
                refined_gps = None

        else:
            logging.info("unable to resolve lockloss time")

    if not refined_gps:
        ts = 'nan'
    else:
        ts = '{:f}'.format(refined_gps)
        event.add_tag('REFINED')

    with open(event.gen_path('refined_gps'), 'w') as f:
        f.write('{}\n'.format(ts))

    logging.info("plotting indicators...")
    plot_indicators(
        event,
        bufs,
        params,
        refined_gps=refined_gps,
        threshold=threshold,
    )

##################################################

def _parser_add_arguments(parser):
    parser.add_argument('event', type=int, nargs='?',
                        help="event ID")


def main(args=None):
    """Refine event time

    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    event = LocklossEvent(args.event)
    refine_event(event)


if __name__ == '__main__':
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    logging.basicConfig(level='DEBUG')
    main()
