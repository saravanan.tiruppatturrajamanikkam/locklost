import glob
import time
import timeit
import logging
import argparse

import gwdatafind
from gwpy.segments import Segment, SegmentList
from lal.utils import CacheEntry

from .. import config
from ..event import LocklossEvent

##################################################

def find_shm_segments():
    """
    Returns a segment list covering the span of available
    frame data from /dev/shm.
    """
    frames = glob.glob("/dev/shm/lldetchar/{}/*".format(config.IFO))
    cache = map(CacheEntry.from_T050017, frames)
    return SegmentList([e.segment for e in cache]).coalesce()


def discover_data(event):
    """
    Discovers sources of data for other plugins,
    if data is not available will wait until there is
    up to a maximum time specified.
    """
    gps = int(event.gps)

    ### use largest data window to determine data query range
    query_segment = Segment(*config.REFINE_WINDOW).shift(gps)
    query_segs = SegmentList([query_segment])
    logging.info("querying for data in range: {} - {}".format(*query_segment))

    ### first check if low-latency frames are available
    logging.info("checking if data is available in /dev/shm...")
    shm_segs = find_shm_segments()
    if query_segs & shm_segs == query_segs:
        shm_end_time = shm_segs[-1][1]
        query_end_time = query_segment[1]

        ### check if time requested is too new, if so,
        ### sleep until it is available
        if query_end_time > shm_end_time:
            time.sleep(query_end_time - shm_end_time)
            shm_segs = find_shm_segments()

        ### check if span of data requested is all available
        if query_segs & shm_segs == query_segs:
            logging.info("queried data available in /dev/shm")
            return True

    ### if shm frames are not available for the time requested,
    ### wait until raw frames from LDR are available
    logging.info("no data available in /dev/shm, checking LDR...")
    elapsed = 0
    start_time = timeit.default_timer()
    while elapsed <= config.DATA_DISCOVERY_TIMEOUT:

        logging.debug('gwdatafind.find_times({}, {}, {}, {})'.format(
            config.IFO[0],
            '{}_R'.format(config.IFO),
            query_segment[0],
            query_segment[1],
        ))
        ldr_segs = gwdatafind.find_times(
            config.IFO[0],
            '{}_R'.format(config.IFO),
            query_segment[0],
            query_segment[1],
        )

        if query_segs & ldr_segs == query_segs:
            logging.info("data available in LDR, starting analysis")
            return

        logging.info(
            "no data available in LDR, sleeping for {} seconds".format(
                config.DATA_DISCOVERY_SLEEP,
            )
        )
        time.sleep(config.DATA_DISCOVERY_SLEEP)

        elapsed = timeit.default_timer() - start_time

    raise RuntimeError("data discovery timeout reached, data not found")

##################################################

def _parser_add_arguments(parser):
    parser.add_argument('event', type=int, nargs='?',
                        help="event ID")


def main(args=None):
    """
    Data discovery
    """
    if not args:
        parser = argparse.ArgumentParser()
        _parser_add_arguments(parser)
        args = parser.parse_args()

    event = LocklossEvent(args.event)
    data = discover_data(event)

    if not data:
        raise RuntimeError('No data available to do lockloss analysis!')


if __name__ == '__main__':
    import signal
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    logging.basicConfig(level='DEBUG')
    main()
