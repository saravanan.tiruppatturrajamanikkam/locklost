import sys
import signal

from . import followups
from .. import event

def main():
    mod = sys.argv[1]
    gps = sys.argv[2]
    followups[mod](event.LocklossEvent(gps))

if __name__ == '__main__':
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    main()

