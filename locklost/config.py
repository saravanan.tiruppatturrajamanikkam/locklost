import os
import sys
from collections import OrderedDict

IFO = os.getenv('IFO')
if not IFO:
    sys.exit('Must specify IFO env var.')

LOG_FMT = '%(asctime)s [%(module)s.%(funcName)s] %(message)s'

EVENT_ROOT = os.getenv('LOCKLOST_EVENT_ROOT', '')
if os.path.exists(EVENT_ROOT):
    EVENT_ROOT = os.path.abspath(EVENT_ROOT)
SEG_DIR = os.path.join(EVENT_ROOT, '.segments')

WEB_ROOT = os.getenv('LOCKLOST_WEB_ROOT', '')
if os.path.exists(WEB_ROOT):
    WEB_ROOT = os.path.abspath(WEB_ROOT)

CONDOR_ONLINE_DIR = os.path.join(EVENT_ROOT, '.condor_online')
CONDOR_SEARCH_DIR = os.path.join(EVENT_ROOT, '.condor_search')
CONDOR_ANALYZE_DIR = os.path.join(EVENT_ROOT, '.condor_analyze')

QUERY_DEFAULT_SHOW = 50

CONDOR_ACCOUNTING_GROUP = os.getenv('CONDOR_ACCOUNTING_GROUP')
CONDOR_ACCOUNTING_GROUP_USER = os.getenv('CONDOR_ACCOUNTING_GROUP_USER')

GRD_NODE = 'ISC_LOCK'
GRD_STATE_N_CHANNEL = '{}:GRD-{}_STATE_N'.format(IFO, GRD_NODE)
GRD_STATE_S_CHANNEL = '{}:GRD-{}_STATE_S'.format(IFO, GRD_NODE)

CA_MONITOR_CHANNEL = GRD_STATE_S_CHANNEL

if IFO == 'H1':
    GRD_NOMINAL_STATE = ('NOMINAL_LOW_NOISE', 600)
elif IFO == 'L1':
    GRD_NOMINAL_STATE = ('LOW_NOISE', 2000)
GRD_LOCKLOSS_STATES = [
    ('LOCKLOSS', 2),
    ('LOCKLOSS_DRMI', 3),
]
LOCKLOSS_DRMI = 3
DOWN = 10

SEARCH_STRIDE = 10000

DATA_ACCESS = os.getenv('DATA_ACCESS', 'gwpy')

MAX_QUERY_LATENCY = 70

DATA_DISCOVERY_SLEEP = 30
DATA_DISCOVERY_TIMEOUT = 600

# tag colors for web (foreground, background)
TAG_COLORS = {
    'OBSERVE': ('black', 'lime'),
    'REFINED': ('white', 'grey'),
}

PLOT_CHANNELS = [
    'LSC-POP_A_LF_OUT_DQ',
    'ASC-X_TR_A_NSUM_OUT_DQ',
    'ASC-Y_TR_A_NSUM_OUT_DQ',
    'OMC-DCPD_SUM_OUT_DQ',
    'ASC-AS_A_DC_SUM_OUT_DQ',

    'SUS-BS_M2_LOCK_L_OUT_DQ',
    'SUS-PRM_M3_LOCK_L_OUT_DQ',
    'SUS-ETMX_L3_LOCK_L_OUT_DQ',
    'SUS-ETMY_L3_LOCK_L_OUT_DQ',
    'SUS-SRM_M3_LOCK_L_OUT_DQ',

    'ASC-CHARD_P_OUT_DQ',
    'ASC-CHARD_Y_OUT_DQ',
    'ASC-DHARD_P_OUT_DQ',
    'ASC-DHARD_Y_OUT_DQ',
    'ASC-CSOFT_P_OUT_DQ',
    'ASC-CSOFT_Y_OUT_DQ',
    'ASC-DSOFT_P_OUT_DQ',
    'ASC-DSOFT_Y_OUT_DQ',

    'ASC-INP2_P_OUT_DQ',
    'ASC-PRC1_P_OUT_DQ',
    'ASC-PRC2_P_OUT_DQ',
    'ASC-SRC1_P_OUT_DQ',
    'ASC-SRC2_P_OUT_DQ',
    'ASC-MICH_P_OUT_DQ',

    'ASC-INP2_Y_OUT_DQ',
    'ASC-PRC1_Y_OUT_DQ',
    'ASC-PRC2_Y_OUT_DQ',
    'ASC-SRC1_Y_OUT_DQ',
    'ASC-SRC2_Y_OUT_DQ',
    'ASC-MICH_Y_OUT_DQ',
]
PLOT_CHANNELS = [GRD_STATE_N_CHANNEL] + ['%s:%s' % (IFO, chan) for chan in PLOT_CHANNELS]

SEARCH_CHANNELS = [GRD_STATE_N_CHANNEL] + ['%s:%s' % (IFO, 'ASC-AS_A_DC_NSUM_OUT_DQ')]

LOCK_STRETCH_WINDOW = [-60, 0]

# per IFO key is the state index threshold for the specified indicator
# params, e.g. for H1, for lock losses from states between 102 and 410
# the indicator parameters will be...
INDICATORS = {
    'H1': {
        410: {
            'CHANNEL': '{}:ASC-AS_A_DC_NSUM_OUT_DQ'.format(IFO),
            'THRESHOLD': 10,
            'MINIMUM': 50,
        },
        102: {
            'CHANNEL': '{}:LSC-POPAIR_B_RF18_I_ERR_DQ'.format(IFO),
            'THRESHOLD': -10,
            'MINIMUM': 50,
        },
        0: {
            'CHANNEL': '{}:IMC-TRANS_OUT_DQ'.format(IFO),
            'THRESHOLD': -10,
            'MINIMUM': 50,
        },
    },
    'L1': {
        0: {
            'CHANNEL': '{}:IMC-TRANS_OUT_DQ'.format(IFO),
            'THRESHOLD': -10,
            'MINIMUM': 50,
        },
    },
}

PLOT_SATURATIONS = 8
SATURATION_THRESHOLD = 131072
PLOT_CHUNKSIZE = 3000

#ETM's are 20-bit and were changed at a date within our wanted lockloss time range
ETMX_L3_CHANNELS = [
    'SUS-ETMX_L3_MASTER_OUT_UR_DQ',
    'SUS-ETMX_L3_MASTER_OUT_UL_DQ',
    'SUS-ETMX_L3_MASTER_OUT_LR_DQ',
    'SUS-ETMX_L3_MASTER_OUT_LL_DQ',
]
ETMX_L3_CHANNELS = ['%s:%s' % (IFO, chan) for chan in ETMX_L3_CHANNELS]

#RM's, OM'S, and ZM'S are 16-bit
SIXTEEN_BIT_CHANNELS = [
    'SUS_RM_M1_MASTER_OUT_UR_DQ',
    'SUS_RM_M1_MASTER_OUT_UL_DQ',
    'SUS_RM_M1_MASTER_OUT_LR_DQ',
    'SUS_RM_M1_MASTER_OUT_LL_DQ',
    'SUS_ZM_M1_MASTER_OUT_UR_DQ',
    'SUS_ZM_M1_MASTER_OUT_UL_DQ',
    'SUS_ZM_M1_MASTER_OUT_LR_DQ',
    'SUS_ZM_M1_MASTER_OUT_LL_DQ',
    'SUS_OM_M1_MASTER_OUT_UR_DQ',
    'SUS_OM_M1_MASTER_OUT_UL_DQ',
    'SUS_OM_M1_MASTER_OUT_LR_DQ',
    'SUS_OM_M1_MASTER_OUT_LL_DQ',
]
SIXTEEN_BIT_CHANNELS = ['%s:%s' % (IFO, chan) for chan in SIXTEEN_BIT_CHANNELS]


# This corresponds to a change from 18 to 20-bit DAC for ETMX L3 channels.
# ETMX L3 channels after this date have counts that are four times higher
ETMX_L3_CHANGE_DATE = 1224961218

LPY_CHANNELS = [
    'SUS-ETMX_L2_MASTER_OUT_UR_DQ',
    'SUS-ETMX_L2_MASTER_OUT_UL_DQ',
    'SUS-ETMX_L2_MASTER_OUT_LR_DQ',
    'SUS-ETMX_L2_MASTER_OUT_LL_DQ',
]
LPY_CHANNELS = ['%s:%s' % (IFO, chan) for chan in LPY_CHANNELS]

SATURATION_CM = ['#332288', '#88CCEE', '#117733', '#999933', '#DDCC77', '#CC6677', '#882255', '#AA4499']
SC_CM = ['#e6194b', '#3cb44b', '#4363d8', '#f58231', '#911eb4', '#46f0f0', '#f032e6', '#bcf60c', '#008080']

ADC_OVERFLOWS = {
    'ASC': {
        'ADC_ID': 19,
        'num_bits': 5,
        'bit_exclude': [],
    },
    'LSC': {
        'ADC_ID': 10,
        'num_bits': 3,
        'bit_exclude': [(2,4), (2,13), (2,15)],
    },
    'OMC': {
        'ADC_ID': 8,
        'num_bits': 3,
        'bit_exclude': [(2,4), (2,13), (2,15)],
    },
}
SAT_SEARCH_WINDOW = [-30, 5]

REFINE_WINDOW = [-60, 10]
REFINE_PLOT_WINDOWS = {
    'WIDE': [-30, 10],
    'ZOOM': [-5, 2],
}

PLOT_WINDOWS = {
    'WIDE': [-30, 10],
    'ZOOM': [-5, 1],
}

FIG_SIZE = [n*0.6 for n in [16, 9]]

GSTLAL_TRIGGER_DIR = '/home/idq/gstlal/online/features'

GLITCH_PLOT_WINDOWS = {
    'WIDE': [-20, 2],
    'ZOOM': [-5, 1],
}

GLITCH_CHANNELS = {
    'ASC_AS': [
        'ASC-AS_A_DC_NSUM_OUT_DQ',
        'ASC-AS_A_DC_PIT_OUT_DQ',
        'ASC-AS_A_DC_YAW_OUT_DQ',
        'ASC-AS_A_RF36_I_PIT_OUT_DQ',
        'ASC-AS_A_RF36_I_YAW_OUT_DQ',
        'ASC-AS_A_RF36_Q_PIT_OUT_DQ',
        'ASC-AS_A_RF36_Q_YAW_OUT_DQ',
        'ASC-AS_A_RF45_I_PIT_OUT_DQ',
        'ASC-AS_A_RF45_I_YAW_OUT_DQ',
        'ASC-AS_A_RF45_Q_PIT_OUT_DQ',
        'ASC-AS_A_RF45_Q_YAW_OUT_DQ',
        'ASC-AS_B_RF36_I_PIT_OUT_DQ',
        'ASC-AS_B_RF36_I_YAW_OUT_DQ',
        'ASC-AS_B_RF36_Q_PIT_OUT_DQ',
        'ASC-AS_B_RF36_Q_YAW_OUT_DQ',
        'ASC-AS_B_RF45_I_PIT_OUT_DQ',
        'ASC-AS_B_RF45_I_YAW_OUT_DQ',
        'ASC-AS_B_RF45_Q_PIT_OUT_DQ',
        'ASC-AS_B_RF45_Q_YAW_OUT_DQ',
    ],
    'ASC_HS': [
        'ASC-CHARD_Y_OUT_DQ',
        'ASC-CSOFT_P_OUT_DQ',
        'ASC-CSOFT_Y_OUT_DQ',
        'ASC-DHARD_P_OUT_DQ',
        'ASC-DHARD_Y_OUT_DQ',
        'ASC-DSOFT_P_OUT_DQ',
        'ASC-DSOFT_Y_OUT_DQ',
        'ASC-MICH_P_OUT_DQ',
        'ASC-MICH_Y_OUT_DQ',
        'ASC-PRC1_Y_OUT_DQ',
        'ASC-PRC2_P_OUT_DQ',
    ],
    'ASC_REFL': [
        'ASC-REFL_A_RF45_I_PIT_OUT_DQ',
        'ASC-REFL_A_RF45_I_YAW_OUT_DQ',
        'ASC-REFL_A_RF45_Q_PIT_OUT_DQ',
        'ASC-REFL_A_RF45_Q_YAW_OUT_DQ',
        'ASC-REFL_A_RF9_I_PIT_OUT_DQ',
        'ASC-REFL_A_RF9_Q_PIT_OUT_DQ',
        'ASC-REFL_A_RF9_Q_YAW_OUT_DQ',
        'ASC-REFL_B_DC_NSUM_OUT_DQ',
        'ASC-REFL_B_DC_PIT_OUT_DQ',
        'ASC-REFL_B_DC_YAW_OUT_DQ',
        'ASC-REFL_B_RF45_I_PIT_OUT_DQ',
        'ASC-REFL_B_RF45_I_YAW_OUT_DQ',
        'ASC-REFL_B_RF45_Q_PIT_OUT_DQ',
        'ASC-REFL_B_RF45_Q_YAW_OUT_DQ',
        'ASC-REFL_B_RF9_I_PIT_OUT_DQ',
        'ASC-REFL_B_RF9_Q_PIT_OUT_DQ',
        'ASC-REFL_B_RF9_Q_YAW_OUT_DQ',
        'ASC-SRC2_P_OUT_DQ',
        'ASC-SRC2_Y_OUT_DQ',
    ],
    'ASC_TR': [
        'ASC-X_TR_A_NSUM_OUT_DQ',
        'ASC-X_TR_A_PIT_OUT_DQ',
        'ASC-X_TR_A_YAW_OUT_DQ',
        'ASC-X_TR_B_NSUM_OUT_DQ',
        'ASC-X_TR_B_PIT_OUT_DQ',
        'ASC-X_TR_B_YAW_OUT_DQ',
        'ASC-Y_TR_A_NSUM_OUT_DQ',
        'ASC-Y_TR_A_PIT_OUT_DQ',
        'ASC-Y_TR_A_YAW_OUT_DQ',
        'ASC-Y_TR_B_NSUM_OUT_DQ',
        'ASC-Y_TR_B_PIT_OUT_DQ',
        'ASC-Y_TR_B_YAW_OUT_DQ',
    ],
    'IMC' : [
        'IMC-DOF_1_P_IN1_DQ',
        'IMC-DOF_2_P_IN1_DQ',
        'IMC-DOF_4_P_IN1_DQ',
        'IMC-F_OUT_DQ',
        'IMC-IM4_TRANS_PIT_OUT_DQ',
        'IMC-IM4_TRANS_SUM_IN1_DQ',
        'IMC-IM4_TRANS_YAW_OUT_DQ',
        'IMC-MC2_TRANS_PIT_OUT_DQ',
        'IMC-PWR_IN_OUT_DQ',
        'IMC-REFL_DC_OUT_DQ',
        'IMC-TRANS_OUT_DQ',
        'IMC-WFS_A_DC_SUM_OUT_DQ',
        'IMC-WFS_A_I_PIT_OUT_DQ',
        'IMC-WFS_A_I_YAW_OUT_DQ',
        'IMC-WFS_A_Q_PIT_OUT_DQ',
        'IMC-WFS_A_Q_YAW_OUT_DQ',
        'IMC-WFS_B_DC_PIT_OUT_DQ',
        'IMC-WFS_B_DC_YAW_OUT_DQ',
        'IMC-WFS_B_I_PIT_OUT_DQ',
        'IMC-WFS_B_Q_PIT_OUT_DQ',
        'IMC-WFS_B_Q_YAW_OUT_DQ',
    ],
    'LSC': [
        'LSC-MCL_IN1_DQ',
        'LSC-MICH_IN1_DQ',
        'LSC-MICH_OUT_DQ',
        'LSC-MOD_RF45_AM_AC_OUT_DQ',
        'LSC-POPAIR_A_RF9_Q_ERR_DQ',
        'LSC-POP_A_LF_OUT_DQ',
        'LSC-POP_A_RF45_I_ERR_DQ',
        'LSC-POP_A_RF9_Q_ERR_DQ',
        'LSC-PRCL_IN1_DQ',
        'LSC-PRCL_OUT_DQ',
        'LSC-REFL_A_LF_OUT_DQ',
        'LSC-REFL_A_RF45_I_ERR_DQ',
        'LSC-REFL_A_RF45_Q_ERR_DQ',
        'LSC-REFL_A_RF9_Q_ERR_DQ',
        'LSC-REFL_SERVO_ERR_OUT_DQ',
        'LSC-Y_ARM_OUT_DQ',
    ],
    'PSL' : [
        'PSL-FSS_PC_MON_OUT_DQ',
        'PSL-ILS_HV_MON_OUT_DQ',
        'PSL-ILS_MIXER_OUT_DQ',
        'PSL-ISS_PDA_REL_OUT_DQ',
        'PSL-ISS_PDB_REL_OUT_DQ',
        'PSL-ISS_SECONDLOOP_QPD_PIT_OUT_DQ',
        'PSL-OSC_PD_AMP_DC_OUT_DQ',
        'PSL-OSC_PD_INT_DC_OUT_DQ',
        'PSL-OSC_PD_ISO_DC_OUT_DQ',
        'PSL-PMC_HV_MON_OUT_DQ',
        'PSL-PMC_MIXER_OUT_DQ',
    ],
}

LSC_ASC_CHANNELS = OrderedDict()
LSC_ASC_CHANNELS['Power Buildups'] = [
    'LSC-POPAIR_B_RF18_I_ERR_DQ',
    'LSC-POPAIR_B_RF90_I_ERR_DQ',
    'LSC-REFL_A_LF_OUT_DQ',
    'LSC-POP_A_LF_OUT_DQ',
    # 'ASC-X_PWR_CIRC_OUT16',
    # 'ASC-Y_PWR_CIRC_OUT16',
]
LSC_ASC_CHANNELS['LSC Control Signals'] = [
    'LSC-MICH_OUT_DQ',
    'LSC-PRCL_OUT_DQ',
    'LSC-SRCL_OUT_DQ',
    'LSC-DARM_OUT_DQ',
    # 'LSC-MICHFF_OUT_DQ',
    # 'LSC-SRCLFF1_OUT_DQ',
    'LSC-MCL_OUT_DQ',
    'LSC-REFL_SERVO_CTRL_OUT_DQ',
    'IMC-L_OUT_DQ',
    'IMC-F_OUT_DQ',
]
LSC_ASC_CHANNELS['LSC Sensors'] = [
    'LSC-POP_A_RF9_I_ERR_DQ',
    'LSC-POP_A_RF9_Q_ERR_DQ',
    'LSC-REFL_A_RF9_I_ERR_DQ',
    'LSC-REFL_A_RF9_Q_ERR_DQ',
    'LSC-POP_A_RF45_I_ERR_DQ',
]
LSC_ASC_CHANNELS['ASC Control Signals (Arm)'] = [
    'ASC-CHARD_Y_OUT_DQ',
    'ASC-CHARD_P_OUT_DQ',
    'ASC-DHARD_Y_OUT_DQ',
    'ASC-DHARD_P_OUT_DQ',
    'ASC-CSOFT_Y_OUT_DQ',
    'ASC-CSOFT_P_OUT_DQ',
    'ASC-DSOFT_Y_OUT_DQ',
    'ASC-DSOFT_P_OUT_DQ',
]
LSC_ASC_CHANNELS['ASC Control Signals (Vertex)'] = [
    'ASC-PRC1_Y_OUT_DQ',
    'ASC-PRC1_P_OUT_DQ',
    'ASC-PRC2_Y_OUT_DQ',
    'ASC-PRC2_P_OUT_DQ',
    'ASC-MICH_Y_OUT_DQ',
    'ASC-MICH_P_OUT_DQ',
    'ASC-SRC1_Y_OUT_DQ',
    'ASC-SRC1_P_OUT_DQ',
    'ASC-SRC2_Y_OUT_DQ',
    'ASC-SRC2_P_OUT_DQ',
    'ASC-INP1_P_OUT_DQ',
    'ASC-INP1_Y_OUT_DQ',
]
LSC_ASC_CHANNELS['ASC Centering Control Signals'] = [
    'ASC-DC1_Y_OUT_DQ',
    'ASC-DC1_P_OUT_DQ',
    'ASC-DC2_Y_OUT_DQ',
    'ASC-DC2_P_OUT_DQ',
    'ASC-DC3_Y_OUT_DQ',
    'ASC-DC3_P_OUT_DQ',
    'ASC-DC4_Y_OUT_DQ',
    'ASC-DC4_P_OUT_DQ',
]
for key in LSC_ASC_CHANNELS:
    LSC_ASC_CHANNELS[key] = ['%s:%s' % (IFO, chan) for chan in LSC_ASC_CHANNELS[key]]
