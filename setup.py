from setuptools import setup


setup(
    setup_requires=[
        'setuptools_scm',
    ],
    use_scm_version={
        'write_to': 'locklost/version.py',
    },

    name = 'locklost',
    description = 'LIGO lock loss tracking and analysis',
    author = 'Jameson Graef Rollins',
    author_email = 'jameson.rollins@ligo.org',
    url = 'https://git.ligo.org/jameson.rollins/locklost.git',
    license = 'GPLv3+',

    packages = [
        'locklost',
        'locklost.plugins',
        'locklost.web',
        'locklost.web.templates',
    ],

    package_data = {
        'locklost.web.templates': ['*.tpl'],
    },

    entry_points={
        'console_scripts': [
            'locklost = locklost.__main__:main',
        ],
    },
)
